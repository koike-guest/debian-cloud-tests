import boto3
import sys
import time

from ..common import CloudTester
from ..common.test_image import *


class AWSCloudTester(CloudTester):
    def __init__(self, profile, region, image_id, security_group, username):
        super().__init__()
        self.session = None
        self.ec2_connection = None

        self.region = region
        self.profile = profile
        self.image_id = image_id
        self.username = username
        self.security_group = security_group

    def connect(self):
        super().connect()
        self.session = boto3.session.Session(region_name=self.region,
                                             profile_name=self.profile)
        self.ec2_connection = self.session.resource("ec2")

    def start_image(self):
        super().start_image()
        self.aws_import_pub_key()
        # try:
        instances = self.ec2_connection.create_instances(
            ImageId=self.image_id,
            MinCount=1,
            MaxCount=1,
            KeyName=self.key_name,
            SecurityGroups=[self.security_group],
            InstanceType="t2.micro")
        self.instance = instances[0]

        ec2_client = self.ec2_connection.meta.client
        waiter = ec2_client.get_waiter('instance_running')
        waiter.wait(InstanceIds=[self.instance.instance_id])

        # except:
        #     print("Got problems starting image")
        #     sys.exit(1)

        # sometimes the waiter returns before public IP is associated
        n = 0
        limit = 10
        while True:
            self.instance.load()
            if self.instance.public_ip_address:
                self.ip = self.instance.public_ip_address
                break
            if n >= limit:
                print('Instance has no public IP after %d polls' % limit)
                sys.exit(1)

            n += 1
            time.sleep(1)

        self.ip = self.instance.public_ip_address
        self.connection = self.connect_to_machine(self.ip, self.username)

    def stop_image(self):
        # cleanup the ephemeral ssh key before shutting down
        self.ec2_connection.KeyPair(self.key_name).delete()

        self.connection.close()
        self.instance.terminate()
        super().stop_image()

    def disconnect(self):
        self.session = None
        self.ec2_connection = None
        super().disconnect()

    def aws_import_pub_key(self):
        self.key_name = 'debian-test-%d' % time.time()
        self.ec2_connection.import_key_pair(
            KeyName=self.key_name,
            PublicKeyMaterial=self.pub_key.encode('utf-8'),
        )
