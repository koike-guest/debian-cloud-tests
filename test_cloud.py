#! /usr/bin/python3
import argparse

from debcloudtests.common import test_root_login

parser = argparse.ArgumentParser(description='Debian cloud image test suite')
subparsers = parser.add_subparsers(dest='provider')

subparser = subparsers.add_parser('aws')
subparser.add_argument('--region', default='eu-central-1')
subparser.add_argument('--security-group', default='SSH')  # todo: support security group id
subparser.add_argument('--key-name')
subparser.add_argument('--key-file')
subparser.add_argument('--username', default='admin')
subparser.add_argument('--image-id')  # ami-ea4fe285
subparser.add_argument('--profile')

subparser = subparsers.add_parser('gce')
subparser.add_argument('--project')
subparser.add_argument('--zone', default='us-central1-b')
subparser.add_argument('--image-name')

args = parser.parse_args()


def test_apt(connection):
    try:
        stdin, stdout, stderr = connection.exec_command("sudo apt update")
        print(stdout.read())
        print(stderr.read())
        return True
    except:
        return False


def test_image_startup(image):
    raise NotImplementedError("No idea how to do it right now")


def test_cloud_init(connection):
    pass


def test_network_drivers(connection):
    raise NotImplementedError("No idea how to do it right now. Provider specific")


def test_size(image):
    return True


if __name__ == '__main__':
    if args.provider == 'aws':
        from debcloudtests.aws import AWSCloudTester
        cloud = AWSCloudTester(args.profile, args.region, args.image_id,
                               args.security_group, args.username)
    elif args.provider == 'gce':
        from debcloudtests.gce import GCECloudTester
        cloud = GCECloudTester(args.project, args.zone, args.image_name)
    # TODO: add else statement

    cloud.connect()
    cloud.start_image()
    # Test image startup

    if test_root_login(cloud):
        print("root login disallowed")
    else:
        print("Strange! we can login as root")

    # stdin, stdout, stderr = connection.exec_command("sudo apt update")
    # print(stdout.read())
    # print(stderr.read())

    cloud.copy_image_test_code("test_image.py")
    if not cloud.run_image_test_code("test_image.py"):
        print("We had problems with running test script on instance")

    cloud.stop_image()
    cloud.disconnect()
